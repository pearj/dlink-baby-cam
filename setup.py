from setuptools import setup

setup(
    name='DlinkBabyCam',
    version='1.0',
    py_modules=['DlinkBabyCam'],
    install_requires=[
        'requests==2.12.4',
        'urllib3[secure]==1.19.1',
        'click==6.7'
    ],
    entry_points='''
        [console_scripts]
        dlink-baby=DlinkBabyCam:cli
    '''
    )