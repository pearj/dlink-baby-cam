'''
Created on 1Apr.,2017

@author: Joel
'''
import requests

class DlinkBabyCamApi(object):
    '''
    classdocs
    '''


    def __init__(self, username, password, baseurl, sslVerify=True):
        '''
        Constructor
        '''
        self.username = username
        self.password = password
        self.baseurl = baseurl
        self.sslVerify = sslVerify
        
    def sendRequest(self, uri, payload):
        r = requests.get(self.baseurl + uri, params=payload, auth=(self.username, self.password))
#         print r.status_code
#         print r.text
        parsed = self._parseOutput(r.text)
        
        return parsed
        
    def _parseOutput(self, text):
        parsedLines={}
        for line in text.split('\n'):
            line = line.strip()
            # Skip null lines
            if not line: continue
            linesplit = line.split('=', 1)
            if len(linesplit) > 1:
                parsedLines[linesplit[0]] = linesplit[1]
        return parsedLines
            
        
    def _parseItems(self, parsed):
        itemArr=[]
        
        items = parsed['items'].strip()
        
        if len(items) > 0:
            for item in items.split(':'):
                token = item.split('|')
                name = token[0];
                valType = token[1]
                if valType == 'd':
                    itemType = 'directory'
                elif valType == 'f':
                    itemType = 'file'
                    
                itemDict={'name': name, 'type': itemType}
                itemArr.append(itemDict)
                
        return {'path': parsed['path'], 'items': itemArr}
        
    def listDirectory(self, name):
        payload = {'type': 'video', 'page': 1, 'pagesize': 100, 'path': name}
        parsed = self.sendRequest("/config/sdcard_list.cgi", payload)
        
        items = self._parseItems(parsed)
        
        return items
        
    def walkDirectories(self, path, applyFunc=None):
        print "listing directory: " + path
        output = self.listDirectory(path)
        
        items = output['items']
        
        if (len(items) > 0):
            toProcess=[]
            for entry in items:
                toProcess.append(entry['name'])
                if entry['type'] == 'directory':
                    self.walkDirectories(path + '/' + entry['name'], applyFunc)
                    #applyFunc(path, entry)
                    
            applyFunc(path, toProcess)
        
        
        
    def deleteItems(self, path, names):
        #?type=video&
        print "deleting path " + path + " names " + str(names)  
        payload={'type': 'video', 'path': path, 'name': ':'.join(names)}
        self.sendRequest('/config/sdcard_delete.cgi', payload)
        
    def recursiveDelete(self, path):
        self.walkDirectories(path, self.deleteItems)