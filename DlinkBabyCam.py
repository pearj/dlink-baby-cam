#!/usr/bin/env python
import click
from dlink.DlinkBabyCamApi import DlinkBabyCamApi

class Config(object):
    
    def __init__(self):
        self.verbose = False

pass_config = click.make_pass_decorator(Config, ensure=True)

@click.group()
@click.option('--username', help="Dlink Camera Username", required=True)
@click.option('--password', help="Dlink Camera Password", required=True)
@click.option('--baseurl', help="Dlink Camera Base URL", required=True)
@click.option('--skipVerify', help="Skip SSL Verification", is_flag=True, default=False)
@click.version_option()
@pass_config
def cli(config, username, password, baseurl, skipverify):
    config.username = username
    config.password = password
    config.baseurl = baseurl
    config.skipverify = skipverify
    
    config.dlink = DlinkBabyCamApi(username=config.username, password=config.password, baseurl=config.baseurl, sslVerify=not config.skipverify)
    
@click.option('--name', help="Directory Name", required=True)
@cli.command(help="List the contents of a directory")
@pass_config
def listDirectory(config, name):
    print "Directory name: " + name
    config.dlink.listDirectory(name)
    
@click.option('--name', help="Directory Name", required=True)
@cli.command(help="Recursively delete a directory structure")
@pass_config
def recursiveDelete(config, name):
    print "Deleting Recursively from directory name: " + name
    config.dlink.recursiveDelete(name)